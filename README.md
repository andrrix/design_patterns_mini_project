# My design patterns project

In this project, I tried to highlight two design patterns: 
#### Builder and Iterator 
I thought of creating a mini application that would log users on a platform. 
Users have a predefined ID and are required to provide their name and email, 
the rest of the information is optional. We can display users, add new user, 
search for a user by name or id, or delete user by name or id.

**NOTE - This is my final project to graduate the Google Java Basic course in the "Atelierul Digital pentru Programatori"

