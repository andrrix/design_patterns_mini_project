package com.user;

import java.util.ArrayList;
//import java.util.Scanner;

public class Main {
    private static ArrayList<User> users = new ArrayList<>();
    private static int userId = 0;
    // private static Scanner s = new Scanner(System.in);
    public static void main(String[] args) {

        User user1 = new User.Builder(userId++, "Andrei")
                .email("andrei@mail.com")
                .phone("0751530843")
                .build();
        users.add(user1);

        user1 = new User.Builder(userId++, "Mihai")
                .email("mihai@mail.com")
                .build();
        users.add(user1);

        user1 = new User.Builder(userId++, "Adelina")
                .email("adelina@mail.com")
                .phone("0744509890")
                .build();
        users.add(user1);

        user1 = new User.Builder(userId++, "Claudiu")
                .email("claudiu@mail.com")
                .phone("0745986123")
                .build();
        users.add(user1);

        user1 = new User.Builder(userId++, "Daniela")
                .email("daniela@mail.com")
                .build();
        users.add(user1);
        Menu.setAtribute(users,userId);
        // try{
        Menu.showOptions();
//         }
//        catch (Exception e){
//            e.printStackTrace();
//        } finally {
//            s.close();
//        }


    }
}

