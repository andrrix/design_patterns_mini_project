package com.user;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Menu {
    private static ArrayList<User> users = new ArrayList<>();
    private static Scanner s = new Scanner(System.in);
    private static int userId = 0;


    public static void setAtribute(ArrayList<User> users, int userId) {
        Menu.users = users;
        Menu.userId = userId;
    }

    public static void setUsers(ArrayList<User> users) {
        Menu.users = users;
    }

    public static ArrayList<User> getUsers() {
        return Menu.users;
    }

    static void showOptions() {
        System.out.println("1.Manage users");
        System.out.println("2.Quit");
        System.out.println("Select an option:");
        int option = s.nextInt();
        switch (option) {
            case 1:
                manageUsers();
                break;
            case 2:
                return;
            default:
                showOptions();
        }
    }

    public static void manageUsers() {
        //TODO a menu for manageUsers
        System.out.println("1.Show all users");
        System.out.println("2.Add a new user");
        System.out.println("3.Delete a user by Name");
        System.out.println("4.Delete a user by Id");
        System.out.println("5.Search an user by Name");
        System.out.println("6.Search an user by Id");
        System.out.println("7.Take me back");
        System.out.println("Select an option:");
        int option = s.nextInt();
        switch (option) {
            case 1:
                showAllUsers();
                break;
            case 2:
                addUser();
                break;
            case 3:
                deleteUserByName();
                break;
            case 4:
                deleteUserById();
                break;
            case 5:
                searchUserByName();
                break;
            case 6:
                searchUserById();
                break;
            case 7:
                showOptions();
                break;
            default:
                manageUsers();
        }
    }


    private static void showAllUsers() {
        //TODO print all users
        try {
            Iterator<User> iterator = users.iterator();
            if (users.size() > 0) {
                while (iterator.hasNext()) {
                    System.out.println(iterator.next());
                }
            } else System.out.println("No users to display!!");
            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void addUser() {
        //TODO add a new user in list
        try {
            System.out.print("Adding a new User...\nGive User name\n:");
            s.nextLine();
            String name = s.nextLine();
            name = name.trim();
            System.out.print("Give User email:");
            String email = s.nextLine();
            email = email.trim();
            if (name.equals("") || email.equals("")) {
                System.out.println("User information invalid!\n" +
                        "Please give all information correctly");
                addUser();
            } else {
                final String enteredName = name;
                boolean existUserName = users.stream().anyMatch(user -> enteredName.equals(user.getName()));
                if (existUserName) {
                    System.out.println("The user you already entered have the same name with " +
                            "an existing user");
                    addUser();
                }
                boolean existUserEmail = users.stream().anyMatch(user -> enteredName.equals(user.getEmail()));
                if (existUserEmail) {
                    System.out.println("The user you already entered have the same email with " +
                            "an existing user");
                    addUser();
                } else {
                    users.add(new User.Builder(userId++, name, email)
                            .phone(getUserPhoneOrNot())
                            .city(getUserCityOrNot())
                            .job(getUserJobOrNot())
                            .highSchool(getUserHighSchoolOrNot())
                            .university(getUserUniversityOrNot())
                            .profileImage(getUserProfileImageOrNot())
                            .hasDrivingLicense(getUserHasDrivingLicense())
                            .hasRelationship(getUserHasRelationship())
                            .hobbies(getUserHobbies())
                            .build());
                    System.out.println("User " + name + " was added successfully.");
                }
            }

            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void deleteUserByName() {
        //TODO delete a user
        try {
            s.nextLine();
            System.out.println("Enter an user's name that you want to delete:");
            String name = s.nextLine();
            if (name.equals("")) {
                System.out.println("Name invalid, enter a name!");
                deleteUserByName();
            } else {
                boolean existName = false;
                Iterator<User> iterator = users.iterator();
                while (iterator.hasNext()) {
                    User user = iterator.next();
                    if (name.equals(user.getName())) {
                        existName = true;
                        users.remove(user);

                        System.out.printf("You delete user %s!%n", name);
                        break;
                    }
                }
                if (!existName)
                    System.out.println("Didn't find a user in list");
            }
            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void deleteUserById() {
        //TODO delete a user
        try {
            s.nextLine();
            System.out.println("Enter an user's id that you want to delete:");
            int id1 = s.nextInt();
            if (id1 < 0) {
                System.out.println("Id invalid, enter an Id!");
                deleteUserById();
            } else {
                boolean existName = false;
                Iterator<User> iterator = users.iterator();
                while (iterator.hasNext()) {
                    User user = iterator.next();
                    if (id1 == user.getId()) {
                        existName = true;
                        users.remove(user);

                        System.out.printf("You delete user %d!%n", id1);
                        break;
                    }
                }
                if (!existName)
                    System.out.println("Didn't find a user in list");
            }
            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void searchUserByName() {
        //TODO search a user by Name
        try {
            s.nextLine();
            System.out.println("Enter an user's name that you want to find:");
            String enteredName = s.nextLine();
            if (enteredName.equals("")) {
                System.out.println("Name invalid, enter a Name!");
                searchUserByName();
            } else {
                boolean existName = false;
                Iterator<User> iterator = users.iterator();
                while (iterator.hasNext()) {
                    User user = iterator.next();
                    if (enteredName.equals(user.getName())) {
                        existName = true;
                        System.out.println("The user you were looking for was found:" + user);
                        break;
                    }
                }
                if (!existName)
                    System.out.println("Didn't find a user in list");
            }
            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void searchUserById() {
        //TODO search user
        try {
            s.nextLine();
            System.out.println("Enter an user's id that you want to find:");
            int id1 = s.nextInt();
            if (id1 < 0) {
                System.out.println("Id invalid, enter an Id!");
                searchUserById();
            } else {
                boolean existName = false;
                Iterator<User> iterator = users.iterator();
                while (iterator.hasNext()) {
                    User user = iterator.next();
                    if (id1 == user.getId()) {
                        existName = true;
                        System.out.println("The user you were looking for was found:" + user);
                        break;
                    }
                }
                if (!existName)
                    System.out.println("Didn't find a user in list");
            }
            manageUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getUserPhoneOrNot() {
        System.out.println("Do you want to introduce the phone?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.println("Give user's phone");
            return s.nextLine();
        }
        return null;
    }

    private static String getUserCityOrNot() {
        System.out.println("Do you want to introduce the city?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.println("Give user's city");
            return s.nextLine();
        }
        return null;
    }

    private static String getUserJobOrNot() {
        System.out.println("Do you want to introduce the job?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.println("Give user's job");
            return s.nextLine();
        }
        return null;
    }

    private static String getUserHighSchoolOrNot() {
        System.out.println("Do you want to introduce details about the high school?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.print("Which high school you finished?");
            return s.nextLine();
        }
        return null;
    }

    private static String getUserUniversityOrNot() {
        System.out.println("Do you want to introduce details about the university?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.print("Which university you graduated from?");
            return s.nextLine();
        }
        return null;
    }

    private static String getUserProfileImageOrNot() {
        System.out.println("Do you want to introduce the profile image?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.println("Give user's profile image url");
            return s.nextLine();
        }
        return null;
    }

    private static boolean getUserHasDrivingLicense() {
        System.out.println("Do you want to introduce details about driving license?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.print("Do you have a driving license? (y/n) ");
            return s.nextLine().startsWith("y");
        }
        return false;
    }

    private static boolean getUserHasRelationship() {
        System.out.println("Do you want to introduce details about relationship?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.print("Do you have a relationship? (y/n) ");
            return s.nextLine().startsWith("y");
        }
        return false;
    }

    private static String[] getUserHobbies() {
        System.out.println("Do you want to introduce details about your hobbies?(y/n)");
        String opt = s.nextLine();
        if (opt.equals("y")) {
            System.out.println("Give how many hobbies you have:");
            int nr = s.nextInt();
            String[] hob = new String[nr];
            System.out.println("Give your hobbies with space:");
            for (int i = 0; i < nr; i++)
                hob[i] = s.next();

            return hob;
        }
        return null;
    }
}






