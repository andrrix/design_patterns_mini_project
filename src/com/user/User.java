package com.user;

import java.util.Arrays;

public class User {
    private int id;
    private String name;
    private String email;
    private String phone;
    private final String city;
    private final String job;
    private final String highSchool;
    private final String university;
    private final String profileImage;
    private final boolean hasDrivingLicense;
    private final boolean hasRelationship;
    private final String[] hobbies;


    public User(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.email = builder.email;
        this.phone = builder.phone;
        this.city = builder.city;
        this.job=builder.job;
        this.highSchool=builder.highSchool;
        this.university =builder.university;
        this.profileImage=builder.profileImage;
        this.hasDrivingLicense=builder.hasDrivingLicense;
        this.hasRelationship=builder.hasRelationship;
        this.hobbies=builder.hobbies;

    }

    static class Builder {
        private final int id;
        private final String name;
        private String email;
        private String phone;
        private String city;
        private String job;
        private String highSchool;
        private String university;
        private String profileImage;
        private boolean hasDrivingLicense;
        private boolean hasRelationship;
        private String[] hobbies;


        public Builder(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder(int id, String name, String email) {
            this.id = id;
            this.name = name;
            this.email = email;
        }

        public Builder email(String email) {
            this.email = email;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder job(String job) {
            this.job = job;
            return this;
        }

        public Builder highSchool(String highSchool) {
            this.highSchool = highSchool;
            return this;
        }

        public Builder university(String univeristy) {
            this.university = univeristy;
            return this;
        }

        public Builder profileImage(String profileImage) {
            this.profileImage = profileImage;
            return this;
        }

        public Builder hasDrivingLicense(boolean hasDrivingLicense) {
            this.hasDrivingLicense = hasDrivingLicense;
            return this;
        }

        public Builder hasRelationship(boolean hasRelationship) {
            this.hasRelationship = hasRelationship;
            return this;
        }

        public Builder hobbies(String[] hobbies) {
            this.hobbies = hobbies;
            return this;
        }


        public User build() {
            return new User(this);
        }
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", city='" + city + '\'' +
                ", job='" + job + '\'' +
                ", highSchool='" + highSchool + '\'' +
                ", university='" + university + '\'' +
                ", profileImage='" + profileImage + '\'' +
                ", hasDrivingLicense=" + hasDrivingLicense +
                ", hasRelationship=" + hasRelationship +
                ", hobbies=" + Arrays.toString(hobbies) +
                '}';
    }
}


